'use strict';

class TZ.Models.ContentModel extends Backbone.Model

	initialize: () ->
		# content_model_view = new TZ.Views.ContentModelView {
		# 	model: @
		# }
		# @.on 'invalid', (model)-> console.log model

	defaults: {
		name: ''
		characteristics: {}
		in_edit_state: false
	}

	validate: (attrs, options) ->
		# console.log attrs.name
		if attrs.name.replace(/\s/g, '') == ""
			return "name is required"

		for characteristic of attrs.characteristics
			# Проверяем существует ли свойство, обязательно ли оно и не имеет ли оно пустого значения
			# Возникает особенность проверки файлов
			if attrs[characteristic]? && attrs.characteristics[characteristic].required && attrs[characteristic].replace(/\s/g, '') == ""
				return "'#{attrs.characteristics[characteristic].view_name}' is required"

	parse: (response, options) ->

		if response.error
			@trigger 'server_vaildation_error', @, response
			return false
			# return false
			# console.log @
			# console.log response
			# console.log options

		texts_arr = []

		response = response[0] if _.isArray response

		# Копируем все характеристики, занесенные в коллекцию, для видоизменения и занесения в модель
		# вместе с внутренними значениями ( innerText )

		characteristics = $.extend true, {}, @collection.characteristics

		# Проходим по всем значениям характеристик для занесения внутренних значений ( innerText )

		for text_field of characteristics
			characteristics[text_field].innerText = undefined

			# Как входные данные мы берем массив объектов, полученный с серврева благодаря relations

			# colum {string} - ключ из которого будут взяты данные для отображения
			# require_char {string} - условие по которому значение характеристики будет занесено в innerText

			colum = undefined
			require_char = undefined
			super_state = undefined

			if characteristics[text_field].relation
				colum = characteristics[text_field].relation.colum_name
				require_char = characteristics[text_field].relation.require
			if characteristics[text_field].state
				super_state = characteristics[text_field].state.super

			# Проверяем, если значение должно быть изображением
			if characteristics[text_field].view_type == 'img'
				if characteristics[text_field].relation
					images_arr = []
					# Данный массив нужен, что бы ассоциировать id картинки с картинкой и возвожностью удалить ее
					images_id_arr = []
					images_super_state_arr = []
					for image in response[text_field]
						# Проверяем есть ли условие, по которому должно заноситься выводимое во вью значение
						if require_char?
							if image[require_char]
								images_arr.push image[colum]
								images_id_arr.push image.id
						else
							images_arr.push image[colum]
							images_id_arr.push image.id
						if super_state?
							# console.log image
							images_super_state_arr.push image[super_state]
							# console.log characteristics[text_field]
							# characteristics[text_field].super_state = image[super_state]
					characteristics[text_field].innerText = images_arr
					characteristics[text_field].img_id = images_id_arr
					if images_super_state_arr?
						characteristics[text_field].super_state = images_super_state_arr
				else
					characteristics[text_field].innerText = response[text_field].replace(/\s/g, '').split(',')
			else
				characteristics[text_field].innerText = response[text_field]

		# Создаем объект, имеющий имя ( это основной компонент каждой модели !!! но его нужно заменить !!! ),
		# а также, набор характеристик с внутренними значениям ( innerText ), которые будут переданы во view
		# console.log characteristics

		obj = {
			id: response.id
			name: response.name
			characteristics: characteristics
		}