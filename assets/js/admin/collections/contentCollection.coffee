'use strict';

class TZ.Collections.ContentCollection extends Backbone.Collection

	model: TZ.Models.ContentModel

	url: ->
    	@.name;
	
	initialize: () ->

	parse: (response)->

		return response.model if response.model

		response
		# console.log @

		# for item, index in response
		# 	texts_arr = []
		# 	for text_field in @texts_name_arr
		# 		texts_arr.push item[text_field]
		# 	@add {
		# 		id: item.id
		# 		titel: item.name
		# 		texts_name: @texts_name_arr
		# 		texts: texts_arr
		# 		position_index: index
		# 	}