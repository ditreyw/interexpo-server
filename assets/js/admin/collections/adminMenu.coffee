'use strict';

class TZ.Collections.AdminMenu extends Backbone.Collection

	model: TZ.Models.AdminMenuItem

	url: '/adminMenu'

	parse: (response)->
		# console.log response
		for item in response
			@add {
				name: item.name
				fields_needed: item.fields_needed
				type: item.type
				fields_needed_json: item.fields_needed_json
			}