window.TZ =
	Models: {}
	Collections: {}
	Views: {}
	Routers: {}
	Helpers: {}
	init: ->
		'use strict'

		window.vent = _.extend {}, Backbone.Events

		TZ.menu = new TZ.Collections.AdminMenu
		TZ.menu.fetch().done( ->
			new TZ.Views.App { collection: TZ.menu }
		)
		# TZ.menu.fetch().done( (response)->
		# 	# for item in response
		# 		# TZ.menu.add {
		# 			# name: item.name
		# 			# fields_needed: item.fields_needed
		# 		# }
		# 	new TZ.Views.App { collection: TZ.menu }
		# )

$ ->
	'use strict'
	TZ.init();