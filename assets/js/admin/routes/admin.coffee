'use strict';

class TZ.Routers.MainAdmin extends Backbone.Router

	routes: {
		"": "adminLogin"
		"*content": "contentCreator"
	}

	initialize: () ->
		Backbone.history.start({root:'/adm'})

	adminLogin: ()->
		default_greeting = new TZ.Views.DefaultGreeting {
			el: $('#content-wr')
		}

	contentCreator: (contentName)->
		vent.trigger "content:show#{contentName}", contentName