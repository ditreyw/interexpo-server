'use strict';

class TZ.Views.ContentView extends Backbone.View

	events: {
	}

	initialize: () ->
		# do @render
		@listenTo @collection, 'add', @addOne
		# @listenTo @collection, 'destroy', @addOne
		# @listenTo @collection, 'reset', @render
		# @collection.on 'sync', @addOne, @

	render: () ->
		# @collection.
		# @.remove()

		# console.log @collection

		contentPanel = new TZ.Views.ContentPanelView { collection: @collection }
		@$el.prepend contentPanel.render().el

		@collection.each @addOne, @
		@

	addOne: (contentItem)->
		contentItem = new TZ.Views.ContentModelView { model: contentItem }
		@$el.append contentItem.render().el
		# console.dir @collection

	