'use strict';

class TZ.Views.DefaultGreeting extends Backbone.View

	template: JST['assets/js/admin/templates/defaultGreeting.ejs']

	tagName: 'div'

	id: 'defaultGreeting-wr'

	className: ''

	events: {
	}

	initialize: () ->
		do @render
		# @listenTo @collection, 'change', @render

	render: () ->
		@$el.html @template()
		$('#content-wr').append @$el