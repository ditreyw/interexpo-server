'use strict';

class TZ.Views.ContentModelView extends Backbone.View

	template: JST['assets/js/admin/templates/contentModelView.ejs']

	tagName: 'div'

	id: ''

	className: 'content__item'

	events: {
		'click .controller__item--expande': 'expande'
		'click .controller__item--collapse': 'collapse'
		'click .controller__item--edit': 'edit_state'
		'click .controller__item--cancel': 'cancle_edit_state'
		'click .controller__item--save': 'save'
		'click .controller__item--delete': 'delete'
		'keyup textarea': 'textAreaAdjust'
		'click .img_delete_controller': 'deleteImg'
		'click .img_main_controller': 'changeToMain'
	}

	initialize: () ->
		# do @render
		@listenTo @model, 'change', @render
		@listenTo @model, 'invalid', @invalidVent
			
	render: () ->

		@$el.html @template(@model.toJSON())

		@

	edit_state: ()->
		@$el.addClass 'editing'
		
		textAreaAdjust = @textAreaAdjust

		@$el.find('textarea').each -> textAreaAdjust @

	cancle_edit_state: ()->
		@$el.removeClass 'editing'

	invalidVent: (model, error)->
		swal {
			type: "error",
			title: "Ошибка!",
			text: "#{error}"
		}

	save: ()->

		formFiles = @$('form :file')

		successHandler = (res)=>

			self = @

			project = {}
			
			project.id = res.id

			console.log project.id

			$.ajax({
				# data: project,
				files: formFiles,
				iframe: true,
				url: "/projectimages/?id=#{res.id}",
				method: "POST",
				processData: false,
				contentType: 'multipart/form-data'
			}).done (data)->
				console.log data
				self.model.fetch().done ()->
					self.cancle_edit_state()

		errorHandler = (model, error)=>
			console.log model
			console.log error
			# alert error.summary
			if error.summary
				swal {
					type: "error",
					title: "Ошибка!",
					text: "#{error.summary}"
				}
			else if typeof error == 'string'
				swal {
					type: "error",
					title: "Ошибка!",
					text: "#{error}"
				}
			else if error.responseText
				swal {
					type: "error",
					title: "Ошибка!",
					text: "#{error.responseText}"
				}
			else
				swal {
					type: "error",
					title: "Ошибка!",
					text: "#{error}"
				}

		values = {}

		_.each @$('form').serializeArray(), (input)->
			values[input.name] = input.value

		@model.on 'server_vaildation_error', errorHandler

		@model.save values, {success: successHandler, error: errorHandler, wait: true}

		# @model.save values, { iframe: true, files: @$('form :file'), data: values, success: successHandler, error: errorHandler, wait: true}

	delete: ()->
		swal {
			title: "Вы точно хотите удалить #{@model.get 'name'}?",
			text: "Вы не сможете восстановить эту запись, если удалите",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#C62828",
			confirmButtonText: "Удалить",
			closeOnConfirm: false
		}, ()=>
			@remove()
			@model.destroy().done ()->
				swal("Удалено!", "Запись была успешно удалена", "success");
		# if confirm "Вы точно хотите удалить #{@model.get 'name'}?"
		# 	@remove()
		# 	@model.destroy()

	textAreaAdjust: (o) ->
		TZ.Helpers.TextAreaAdjust(o)

	expande: ()->
		@$el.addClass 'expanded'
		# console.log @$('.content__item__text')

	collapse: ()->
		@$el.removeClass 'expanded'

	deleteImg: (e)->
		image_id = $(e.currentTarget).attr('data-imgId')

		delete_img_ellement = ()=>
			@$(".img_container[data-imgId='#{image_id}']").remove()

		$.ajax({
			url: '/projectimages/'+image_id
			method: "DELETE"
		}).done( (msg) => delete_img_ellement() )

	changeToMain: (e)->
		image_id = $(e.currentTarget).attr('data-imgId')

		$.ajax({
			url: '/projectimages/'+image_id
			method: "POST"
			data: {"mainImg": true}
		}).done( (msg) => @model.fetch() )
