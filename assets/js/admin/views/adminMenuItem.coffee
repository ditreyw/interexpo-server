'use strict';

class TZ.Views.AdminMenuItem extends Backbone.View

	template: JST['assets/js/admin/templates/adminMenuItem.ejs']

	tagName: 'li'

	id: ''

	className: 'adminMenu__item'

	events: {
		'click': 'openContent'
	}

	initialize: () ->
		name = @model.get 'name'
		vent.on "content:show#{name}", @createContent, @

	render: () ->
		@$el.html @template(@model.toJSON())
		@

	createContent: (contentName)->
		console.log "Vent is working : #{contentName}"

		@setActiveMenuItem()

		json_fields = @model.get('fields_needed_json')
		texts_name_arr = []
		characteristics = json_fields
		for name of json_fields
			texts_name_arr.push name

		# texts_name_arr = @model.get('fields_needed').replace(/\s+/g, '').split(',')
		content_req_name = @model.get 'name'

		content_collection = new TZ.Collections.ContentCollection
		content_collection.name = content_req_name
		content_collection.texts_name_arr = texts_name_arr
		content_collection.characteristics = characteristics

		# $('#content-wr').removeClass('activated').addClass 'deactivated'

		$('#content-wr').velocity { opacity: 0 }, { visibility: "hidden" }

		setTimeout ( ->
			$('#content-wr').empty()
			content_collection.fetch().done ->
				contentView = new TZ.Views.ContentView {
					collection: content_collection
					el: '#content-wr'
				}
				contentView.render()
				# $('#content-wr').removeClass('deactivated').addClass('activated')
				$('#content-wr').velocity { opacity: 1 }, { visibility: "visible" }
		), 500


	setActiveMenuItem: (e)->
		$("."+@$el[0].classList[0]).removeClass 'active'
		@$el.addClass 'active'
