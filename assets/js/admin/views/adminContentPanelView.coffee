'use strict';

class TZ.Views.ContentPanelView extends Backbone.View

	template: JST['assets/js/admin/templates/adminContentPanel.ejs']

	tagName: 'div'

	id: ''

	className: 'col-sm-24 content__panel'

	events: {
		'click .controller--content-panel__item--create': 'createItemForm'
		'click .controller--content-panel__item--refresh': 'refreshPage'
		# 'click .controller__item--cancle': 'cancle_edit_state'
		# 'click .controller__item--save': 'save'
		# 'keyup textarea': 'textAreaAdjust'
	}

	initialize: () ->

	render: () ->
		# Для ContentPanel мы создаем отдельный объект, в котором будет хранится нужная нам информация
		panelInfo = {}
		# Имя Content
		panelInfo.name = @collection.name

		@$el.html @template(panelInfo)

		@


	createItemForm: ->
		# console.log @collection.texts_name_arr
		# При нажатии создаем новою модель контента и добавляем в коллекцию
		# new_content_model = @collection.add {
		# 	texts_name: @collection.texts_name_arr
		# }

		new_content_model = new TZ.Models.ContentModel {
			characteristics: @collection.characteristics
		}
		new_content_model.urlRoot = '/'+@collection.url()
		# console.log new_content_model.set {url: '/'+@collection.url()}

		# Создаем для нее отображение формы (так как само отображение контента нас не интересует)
		new_content_model_form = new TZ.Views.ContentModelView  { model: new_content_model }
		# Сразу отображаем состояние редактирования
		new_content_model_form.edit_state()
		new_content_model_form.expande()
		# Выделяем елемент для привязки своих обработчиков, перезаписывающие стандартные
		new_content_model_form_element = new_content_model_form.render().$el

		new_content_model_form.$('.controller__item--delete').remove()

		new_content_model_form.$('.controller__item--cancel').click ()=>
			new_content_model_form.remove()
			new_content_model.destroy()

		new_content_model_form.$('.controller__item--save').click ()=>
			if new_content_model_form.$(':file').val() == ''
				swal {
					type: "error",
					title: "Ошибка!",
					text: "Нет изображения"
				}
				return false
			@collection.add new_content_model

		# Также добавляем эллементу класс для того, что он правильно отображался в форме
		new_content_model_form_element.addClass 'content__item--content-panel'
		#Добавляем в обертку для форм 
		@$('.content-panel__form-wr').prepend new_content_model_form_element
		# После того, как прошло удачное сохранение на сервере, мы удаляем форму из ContentPanel
		new_content_model.on 'sync', =>
			new_content_model_form.remove()

	textAreaAdjust: (o) ->
		# TZ.Helpers.TextAreaAdjust(o)

	refreshPage: ->
		@collection.fetch({remove: false})
