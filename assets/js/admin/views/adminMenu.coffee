'use strict';

class TZ.Views.AdminMenu extends Backbone.View

	tagName: 'ul'

	id: 'adminMenu'

	className: ''

	events: {
	}

	initialize: () ->
		# do @render
		@listenTo @collection, 'change', @render

	render: () ->
		@collection.each @addOne, @
		@

	addOne: (menuItem)->
		menuItem = new TZ.Views.AdminMenuItem { model: menuItem }
		@$el.append menuItem.render().el