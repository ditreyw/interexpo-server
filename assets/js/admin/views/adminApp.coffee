'use strict';

class TZ.Views.App extends Backbone.View

	events: {
	}

	initialize: () ->

		TZ.Helpers.TextAreaAdjust = (o) ->
			target = o.target || o
			target.style.height = "1px"
			target.style.height = (25+target.scrollHeight)+"px"

		menuView = new TZ.Views.AdminMenu {
			collection: @collection
		}

		$('#nav__sidebar').append menuView.render().el

		class MenuOpenner
			constructor: (@$el, @$overlay)->
				@extended = false

				@init()

			exec: ()->

				if !@extended
					@open()
				else
					@close()

			open: ()->

				$('#nav__overlay').velocity("fadeIn", { duration: 500 })
				@$el.addClass 'nav__bar--extended'
				sidebar = $('#nav__sidebar')
				sidebar.css('display', 'block')
				sidebar.find('.starting-point__filler').velocity({
					# transform: 'translateZ(1) scale(1)',
					scale: 1,
					width: '3000px',
					height: '3000px',
					left: '-1500px',
					top: '-1500px'
				})
				$('#adminMenu, #nav__filler').velocity("fadeIn", { duration: 500, delay: 300 })

				setTimeout ()=>
					@extended = true
				, 1000

			close: ()->

				$('#nav__overlay').velocity("fadeOut", { duration: 400 })
				@$el.removeClass 'nav__bar--extended'
				sidebar = $('#nav__sidebar')
				sidebar.find('.starting-point__filler').velocity({
					scale: 0,
				}, { delay: 200 })
				$('#adminMenu, #nav__filler').velocity("fadeOut", { duration: 200 })
				setTimeout ()=>
					sidebar.css('display', 'none')
					@extended = false
				, 1000


			init: ()->

				self = @

				@$el.click (e)->

					e.preventDefault()

					self.exec()

					false

				@$overlay.click (e)->

					e.preventDefault()

					self.close()

					false

		menuOpenner = new MenuOpenner $('#nav__bar'), $('#nav #nav__overlay')

		class UserInfoModal

			constructor: (@$el, @$modalEl)->
				@open()

			open: ()->
				@$el.click (e)=>
					e.preventDefault()
					@$modalEl.velocity 'fadeIn', {duration: 300}
					@$modalEl.mouseleave ()->
						$(@).velocity 'fadeOut', {duration: 300}

		userInfoModal = new UserInfoModal $('#nav__user__user-icon'), $('#nav__user__modal')


		TZ.router = new TZ.Routers.MainAdmin


	render: () ->
		