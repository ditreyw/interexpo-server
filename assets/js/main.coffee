'use strict'

###
INITIAL
###
sheet = document.styleSheets[0]

mediator = new Mediator

$('img, .service-section-block-bg,.inner-portfolio-section-project').lazyload({
    threshold : 200
});

mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && $(window).innerWidth() < 935

addCSSRule = (selector, rules) ->
	if ('insertRule' of sheet)
      sheet.insertRule("#{selector} { #{rules} }", sheet.cssRules.length)
     else if ('addRule' of sheet)
      sheet.addRule(selector, rules, sheet.cssRules.length)

centerDivAbsolute = (selector, horizontal) ->

	the_rules = (selector)->
		element_width = $(selector).width()
		element_height = $(selector).height()

		addCSSRule(selector, "
		 height: #{element_height}px;
		 top: 0;
		 bottom: 0;
		 left: 0;
		 right: 0;
		 margin: auto;
		 position: absolute;")

		addCSSRule(selector, "width: #{element_width}px;") if horizontal

	if typeof selector == "object"
		for element in selector
			the_rules(element)
	else
		the_rules(selector)

centerDivMargin = (selector) ->

	the_rules = (selector)->
		element_width = $(selector).width()
		element_height = $(selector).height()

		addCSSRule(selector, "
		 height: #{element_height}px;
		 top: 50%;
		 margin-top: -#{element_height/2}px;
		 position: relative;")

	if typeof selector == "object"
		for element in selector
			the_rules(element)
	else
		the_rules(selector)

if mobile
	plusTop = -50
else
	plusTop = 5

scrollToAnchor = (aim, callback)->
    aTag = $("##{aim}")
    $('html,body').animate({scrollTop: aTag.offset().top + plusTop},'slow', ()-> callback() if callback?)

# MAIN

window_height = $(window).innerHeight()
window_width = $(window).innerWidth()

port_width = $('.portfolio-section-project').width()
$('.portfolio-section-project').height(port_width)

windowLoaded = false

$(window).load ->

	windowLoaded = true

	$('#emailSenderBtn').click ()->

		email = { 'email': "#{$('#email-placeholder').val()}" }

		if email.email == ''
			alert "Вы ничего не ввели!"
		else
			$('#loaderModal').removeClass 'deactive'
			$.ajax({
				url: '/sendmail'
				method: 'POST'
				data: email
				success: ()->
					$('#email-placeholder').val('')
					$('#loaderModal').addClass 'deactive'
					alert 'Ваша почта успешно отправлена! Ожидайте каталог!'
				error: ()->
					alert 'Приносим извинения у нас проблема с отправкой письма! Попробуйте позже!'
			})

	$('#contactSenderBtn').click ()->

		data = { 'tel': "#{$('#tel-placeholder').val()}", 'name': "#{$('#name-placeholder').val()}" }

		if data.tel == '' && data.name == ''
			alert "Вы неправильно ввели данные"
		else
			$('#loaderModal').removeClass 'deactive'
			$.ajax({
				url: '/sendmail'
				method: 'POST'
				data: data
				success: ()->
					$('#tel-placeholder').val('')
					$('#name-placeholder').val('')
					$('#loaderModal').addClass 'deactive'
					alert 'Ваши данные успешно отправены! Ожидайте звонка!'
				error: ()->
					alert 'Приносим извинения у нас проблема с отправкой данных! Попробуйте позже!'
			})

	if !mobile

		$('#certificate-link').unbind('click').click (e)->
			do e.preventDefault
			img = $('<img>', {
				id: 'certificate-modal-window-image',
				src: '../images/about2.jpg'
				})
			modal = $('<div/>', {
				id: 'certificate-modal-window',
				class: 'deactive'
				})
			modal.append img
			$('body').append modal
			modal.click ->
				modal.removeClass('active').addClass('deactive')
				setTimeout (-> modal.remove()), 300
			setTimeout (-> modal.removeClass('deactive').addClass('active')), 100

		animation = new fadeIn_blocks_animation('.animation-sec')
		animation.init()

		$('.clients-slider-controller').unbind('click').click ->
			$('.clients-square-slider').toggleClass('next-list')

		portfolio_hover_window = new Portfolio_hover_window '.hover-portfolio-section-project'
		portfolio_hover_window.hover_effect()

		service_block = new service_section_blocks $('.service-section-block')
		service_block.set_active_background()
		service_block.set_background_height()

		sertificate_sec_height = $('.sertificate-section').height()
		$('.sertificate-section > div').height(sertificate_sec_height*0.7)

		timer = ""
		$body = $('body')

	menu = new Menu $("nav"), $(".menu-list-item-link"), 'active-link'
	do menu.set_scroll_animation

	$.ajax({
		url: '/projectimages',
		method: 'GET'
	}).done (data)->
		images = {}
		for image_obj in data
			project_name = image_obj.project_id.name.toLowerCase().replace /\s+/g, ''
			if !images[project_name]
				images[project_name] = []
				images[project_name].push image_obj.image
			else
				images[project_name].push image_obj.image

		project_modal = new Project_modal('.inner-portfolio-section-project', '.close-project', images)
		do project_modal.init
	# project_modal = new Project_modal('.inner-portfolio-section-project', '.close-project', $('.prev-project-btn'), $('.next-project-btn'))


	set_DOM_height = ->
			target = $('.main-WR')
			gone_throw = []

			while target.children().length > 0
				target = target.children()
				gone_throw.push target

			while gone_throw.length > -1 and target?
				$.each target, ->
					if $(this)[0].clientHeight >= $(this)[0].scrollHeight and $(this).css('display') != 'none'
						h = $(this)[0].clientHeight
					else if $(this)[0].clientHeight < $(this)[0].scrollHeight and $(this).css('display') != 'none'
						h = $(this)[0].scrollHeight
					$(this).height(h)
				target = gone_throw[gone_throw.length-1]
				gone_throw.splice gone_throw.length-1

	# $('.portfolio-section-text').height(window_height*0.5)

	if mobile
		# set_DOM_height()
		$('.service-section-block').height(window_height/2)
		$('.logo-WR, .menu-list, .menu-controllers').addClass 'deactive'
		do menu.open_mobile_menu

	if $('.main-WR').width()/window.innerHeight < 2
		$('video').css('height', '100%')
	else
		$('video').css('width', '100%')

	video_pos_x = ($('video').width()-$('.main-WR').width())/-2
	video_pos_y = ($('video').height()-$('.main-WR').height())/-2

	$('video').css({
		'top' : video_pos_y,
		'left' : video_pos_x
	})

	$('.service-section-block .overlay').height '100%'

	do menu.set_active_on_scroll if !mobile

	# scrl = $('body').scrollTop()*0.99

	# $('html,body').animate({scrollTop: scrl}, 500)

	setTimeout (-> $(window).scrollTop(0) ), 0

	an = false

	$.fn.mousewheel = (handler)->
		target = $(@)[0]
		if target.addEventListener
			target.addEventListener("mousewheel", handler, false);
			target.addEventListener("DOMMouseScroll", handler, false);
		else
			target.attachEvent("onmousewheel", handler);
		@

	$('body').mousewheel (e)->
		if window.pageYOffset < window_height-20
			do e.preventDefault
		e = window.event || e;
		delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail || -e.originalEvent.detail)));
		if window.pageYOffset < window_height-20 and delta > 0 and !an
			an = true
			scrollToAnchor('first-section-float-emulation', -> an = false)
		if window.pageYOffset < window_height-20 and delta < 0 and !an
			an = true
			scrollToAnchor('about-company-section', -> an = false)

# Only if loading async
checkOfWindowComplition = setInterval( ()->
	if (document.readyState == "complete" && !windowLoaded)
		clearInterval checkOfWindowComplition
		$(window).load()
, 10 )

class Menu
	constructor: (@$menu_element, @$nav_link, @active_menu_link_class)->
		@active_link = false
		@counter = 0

	set_scroll_animation: ->
		self = @
		@$nav_link.unbind('click').click (event)->
			do event.preventDefault
			scrollToAnchor "#{$(@).attr('name')}"
			$(self).trigger 'menu-clicked'

	set_active_on_scroll: ->
		self = @
		section_names = []
		$('.menu-list-item-link').each -> section_names.push $(@).attr 'name'
		section_elements_offsets = []
		link_elements = self.$nav_link.toArray()

		for name in section_names
			section_elements_offsets.push $(".main-WR ##{name}").offset().top


		$(window).scroll (event)->
			# SET FIRST SECTION scrollToAnchor
			if window.pageYOffset > section_elements_offsets[self.counter]
				if self.counter > 0
					$(link_elements[self.counter-1]).removeClass 'active-link'

				$(link_elements[self.counter]).addClass 'active-link'
				# self.active_link = $(link_elements[self.counter])
				self.counter++

			else if window.pageYOffset < section_elements_offsets[self.counter-1]

				$(link_elements[self.counter-1]).removeClass 'active-link'
				$(link_elements[self.counter-2]).addClass 'active-link'

				self.counter--

	open_mobile_menu: ->
		self = @
		$('.mobile-menu').unbind('click').on 'click', ->
			do event.preventDefault
			$('.logo-WR, .menu-list, .menu-controllers').removeClass 'deactive'
			self.$menu_element.addClass 'activated-menu'
			$(@).unbind('click').click ->
				do self.close_mobile_menu
			self.$nav_link.unbind('click').click (event)->
				do event.preventDefault
				scrollToAnchor "#{$(@).attr('name')}"
				$(self).trigger 'menu-clicked'
				do self.close_mobile_menu


	close_mobile_menu: ->
		self = @
		$('.logo-WR, .menu-list, .menu-controllers').addClass 'deactive'
		self.$menu_element.removeClass 'activated-menu'
		do self.open_mobile_menu



class service_section_blocks
	constructor: (@$element) ->
		@element_height = @$element.height()
		@element_width = @$element.width()

	set_height: (height)->
		@$element.height height

	set_background_height: ->
		self = @
		@wr_width = $('.service-section-blocks-WR').width()
		wrHeight = window_height
		@$element.find('.service-section-block-bg').css('background-size', "#{@wr_width}px #{wrHeight}px")

		@$element.hover ->
			$(@).addClass "service-section-hovered"
		, ->
			$(@).removeClass "service-section-hovered"

	set_active_background: ->
		self = @

		@$element.unbind('click').click ->
			$cliked = $(@)
			self.$element.fadeOut( ->
				$cliked.width self.wr_width
				$cliked.height window_height
				$cliked.addClass('activated').fadeIn (->
					$cliked.unbind('click').click ->
						$cliked.fadeOut(->
							$cliked.removeClass('activated').width('50%').height self.element_height
							self.$element.fadeIn()
							self.set_active_background()
						)
				)
			)
			true

class Portfolio_hover_window
	constructor: (element) ->
		@element = element;
		@$element_parent = $(@element).parent()
		@modal_height = $(@element).height()
		@modal_width = $(@element).width()

	hover_effect: ->
		self = @

		mousemove = (event)->
			direction = get_dir(@, { x : event.pageX, y : event.pageY })
			style = get_style(direction)
			$el = $(self.element, @)

			if event.type == 'mouseenter'
				$el.hide().css(style.from)

				$el.show 0, ->
					$(@).stop().animate(style.to, 300)
			else
				$el.stop().animate(style.from, 300)

		get_dir = (el, coordinates) ->

				w = self.$element_parent.width()
				h = self.$element_parent.height()

				x = ( coordinates.x - $(el).offset().left - ( w/2 )) * ( if w > h then ( h/w ) else 1 )
				y = ( coordinates.y - $(el).offset().top  - ( h/2 )) * ( if h > w then ( w/h ) else 1 )

				direction = Math.round( ( ( ( Math.atan2(y, x) * (180 / Math.PI) ) + 180 ) / 90 ) + 3 ) % 4

		get_style = (direction) ->
			fromStyle
			toStyle
			slideFromTop = { left : '0px', top : '-200%' }
			slideFromBottom = { left : '0px', top : '200%' }
			slideFromLeft = { left : '-200%', top : '0px' }
			slideFromRight = { left : '200%', top : '0px' }
			slideTop = { top : '0px' }
			slideLeft = { left : '0px' }

			switch direction
				when 0
					fromStyle = slideFromTop
					toStyle = slideTop
					# console.log 'top'
				when 1
					fromStyle = slideFromRight
					toStyle = slideLeft
					# console.log 'right'
				when 2
					fromStyle = slideFromBottom
					toStyle = slideTop
					# console.log 'bottom'
				when 3
					fromStyle = slideFromLeft
					toStyle = slideLeft
					# console.log 'left'

			{ from : fromStyle, to : toStyle }

		@$element_parent.on 'mouseenter mouseleave', mousemove


class Project_modal
	constructor: (@element, @close_btn, @project_modal_images)->
		@names_obj = {}
		project_elements = $('.inner-portfolio-section-project')
		for proj,index in project_elements
			@names_obj[$(proj).data 'project'] = $(proj).data 'project-img-number'
		@activated_project_name = ''
		@activated_project_text = ''
		@$active_project_image
		# @image_controllers = new Controllers_functionality('.modal-window-image', @$prev_btn, @$next_btn, @set_next_image, @set_prev_image)
		@project_queu = []
		@winOffset = undefined

	init: ->
		self = @
		# for project,number of project_modal_images
		for project,number of @names_obj
			self.project_queu.push project
		do @open_modal

	open_modal: ->
		self = @
		$(@element).unbind('click').click ->
			@winOffset = $(@).offset().top
			$('.main-WR').addClass 'modal-open'
			self.activated_project_name = $(@).data().project
			self.activated_project_text = '.modal-window-text#'+self.activated_project_name
			$('.modal-window').addClass 'active'
			setTimeout ( ->
				do self._set_project_change_queu
				do self.create_images
				$(self.activated_project_text).removeClass('deactive').addClass 'active'
			), 320
			$('html').css 'overflow', 'hidden'
			# self.image_controllers.init(self.$active_project_image, '.modal-window-image')
		do @_close_modal
		do @_change_project
		true

	_close_modal: ->
		self = @
		$(@close_btn).unbind('click').click ->
			$('.main-WR').removeClass 'modal-open'
			$(window).scrollTop(self.winOffset)
			$(self.activated_project_text).removeClass('active').addClass 'deactive'
			$('.modal-window').removeClass('active')
			$('html').css 'overflow', 'initial'
			# $('.modal-window-image').remove()
			$('.modal-window-image-wr').empty()
		true

	set_next_image: (element)->
		element.removeClass('deactive').addClass('active')

	set_prev_image: (element)->
		element.removeClass('active').addClass('deactive')

	create_images: ->
		self = @

		# images_length = project_modal_images[self.activated_project_name]
		# images_length = @names_obj[self.activated_project_name]
		images_length = @project_modal_images[self.activated_project_name].length

		for image in @project_modal_images[self.activated_project_name]

			modal_window_image_wr_class = 'inner-modal-window-image-wr active'
			modal_window_image_class = 'modal-window-image'

			modal_window_image_wr = $('<div/>', {
				class: modal_window_image_wr_class
			})

			modal_window_image = $('<img/>', {
				class: modal_window_image_class,
				src: "../images/Projects/#{image}"
			})

			modal_window_image_wr.append modal_window_image
			$('.modal-window-image-wr').append modal_window_image_wr
			# self.$active_project_image = modal_window_image if image_num == 1

		# for image_num in [1..images_length]
		# 	modal_window_image_wr_class = 'inner-modal-window-image-wr active'
		# 	modal_window_image_class = 'modal-window-image'

		# 	modal_window_image_wr = $('<div/>', {
		# 		class: modal_window_image_wr_class
		# 	})

		# 	modal_window_image = $('<img/>', {
		# 		class: modal_window_image_class,
		# 		src: "../images/Projects/#{self.activated_project_name}#{image_num}.jpg"
		# 	})

		# 	modal_window_image_wr.append modal_window_image
		# 	$('.modal-window-image-wr').append modal_window_image_wr
		# 	self.$active_project_image = modal_window_image if image_num == 1

		true

	_set_project_change_queu: ->
		self = @
		index = @project_queu.indexOf @activated_project_name
		if index == 0
			$('.inner-modal-window-text-controller.prev-project').data('project', self.project_queu[self.project_queu.length-1])
			$('.inner-modal-window-text-controller.next-project').data('project', self.project_queu[1])
		else if index == @project_queu.length-1
			$('.inner-modal-window-text-controller.prev-project').data('project', self.project_queu[index-1])
			$('.inner-modal-window-text-controller.next-project').data('project', self.project_queu[0])
		else
			$('.inner-modal-window-text-controller.prev-project').data('project', self.project_queu[index-1])
			$('.inner-modal-window-text-controller.next-project').data('project', self.project_queu[index+1])

	_change_project: ->
		self = @

		$('.inner-modal-window-text-controller.prev-project, .inner-modal-window-text-controller.next-project').unbind('click').click ->
			$(self.activated_project_text).removeClass('active').addClass 'deactive'
			self.activated_project_name = $(@).data().project
			self.activated_project_text = '.modal-window-text#'+self.activated_project_name
			$(self.activated_project_text).removeClass('deactive').addClass 'active'
			# $('.modal-window-image').remove()
			$('.modal-window-image-wr').empty()
			do self.create_images
			# self.image_controllers.init(self.$active_project_image, '.modal-window-image')
			do self._set_project_change_queu

class fadeIn_blocks_animation
	constructor: (@el)->
		@$el = $(@el)
		@el_array = []
		@el_y_pos = []

	init: ->
		self = @

		$.each @$el, ->
			# self.el_array.push $('>div',@)
			self.el_y_pos.push $(@).offset().top - window_height*0.50

		$(@$el).addClass('transition before-animated')

		do @onscroll_animation


	onscroll_animation: ->
		self = @

		@next_offset_counter = 0
		@prev_offset_counter = -1

		fadeIn_animation = ()->
			self.next_section_offset = self.el_y_pos[self.next_offset_counter]
			self.prev_section_offset = self.el_y_pos[self.prev_offset_counter]

			if window.pageYOffset > self.next_section_offset

				$(self.$el[self.next_offset_counter]).removeClass('before-animated').addClass 'animated'
				self.next_offset_counter++
				self.prev_offset_counter++

			else if window.pageYOffset < self.prev_section_offset

				self.next_offset_counter--
				self.prev_offset_counter--

		do fadeIn_animation

		$(window).scroll (event)->
			do fadeIn_animation
