###
User.coffee

@description :: TODO: You might write a short summary of how this model works and what it represents here.
@docs        :: http://sailsjs.org/#!documentation/models
###

module.exports =

	tableName: 'users'
	migrate: 'safe'
	attributes: {
		email: {
			type: 'email'
		}
		password: {
			type: 'string',
			unique: true
		}
	}

