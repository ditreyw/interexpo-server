###
Projects.coffee

@description :: TODO: You might write a short summary of how this model works and what it represents here.
@docs        :: http://sailsjs.org/#!documentation/models
###

module.exports =
	tableName: 'projects'
	migrate: 'safe'
	autoCreatedAt: false
	autoUpdatedAt: false
	attributes: {
		id: {
			type: 'integer'
			autoIncrement: true
			unique: true
			primaryKey: true
		}
		name: {
			type: "string"
			required: true
			unique: true
		}
		subName: {
			type: "string"
			required: true
		}
		fullName: {
			type: "string"
			required: true
		}
		place: {
			type: "text"
		}
		description: {
			type: "text"
		}
		mainImg: {
			collection: 'projectImages'
			via: 'project_id'
			dominant: true
		}
		images: {
			collection: 'projectImages'
			via: 'project_id'
			dominant: true
		}
	}

