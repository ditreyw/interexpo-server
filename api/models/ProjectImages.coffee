###
ProjectImages.coffee

@description :: TODO: You might write a short summary of how this model works and what it represents here.
@docs        :: http://sailsjs.org/#!documentation/models
###

module.exports =

	# schema: true
	# connection: 'mysql'
	tableName: 'projectsImages'
	attributes: {
		project_id: {
			model: 'projects'
		}
		image:{
			type: 'text'
		}
		mainImg:{
			type: 'boolean'
		}
		createdAt: {
			type: 'datetime',
			defaultsTo: ()-> return new Date()
		}
		updatedAt: {
			type: 'datetime',
			defaultsTo: ()-> return new Date()
		}
	}

