###
ProjectImagesController

@description :: Server-side logic for managing projectimages
@help        :: See http://links.sailsjs.org/docs/controllers
###

fs = require('fs')
path = require('path')
uuid = require('node-uuid')

ProjectImagesController =

	destroy: (req, res, next)->

		id = req.param('id')

		sails.log.info "Destroying images"

		if !id
			res.set('Content-Type', 'text/html; charset=utf-8')
			return res.badRequest('No id provided.')

		ProjectImages.findOne(id).exec (err, result)->
			return res.serserError(err) if err

			return res.notFound() if !result

			imageName = result.image

			sails.log.info imageName

			fs.unlinkSync("#{__dirname}/../../assets/images/Projects/"+imageName)

			ProjectImages.destroy id, (err)->

				return next(err) if err

				res.json(result)

	update: (req, res, next)->

		criteria = {}

		criteria = _.merge {}, req.params.all(), req.body

		id = req.param('id')
		mainImg = req.param 'mainImg'

		if !id
			res.set('Content-Type', 'text/html; charset=utf-8')
			return res.badRequest('No id provided.')

		setMainImg = ()->

			ProjectImages.update id, criteria, (err, result)->

				if err
					return next err

				res.status 201
				res.json result

		ProjectImages.findOne(id).populateAll().exec (err, result)->

			return res.serserError(err) if err
			return res.notFound() if !result

			project_id = result.project_id.id

			if mainImg

				ProjectImages.find {"project_id": project_id}, (err, result)->

					return next(err) if err
					return res.notFound() if result.length == 0

					# Перед тем, как назначать главное изображение, требуется поставить всем значение в false,
					# а после назначить нужное изображение
					# ! По хорошему требуется назначить false всем, КРОМЕ нужного изображения
					ProjectImages.update {"project_id": project_id}, {"mainImg": false}, (err, result)->

						console.log result
						return next(err) if err
						return res.notFound() if result.length == 0

						setMainImg()
			else
				res.status 200
				res.send 'Nothing to update'

	# 'createImages': ()
	create: (req, res, next)->

		project_id = req.param('id')
		uploadFile = req.file('uploadFile')

		if uploadFile.isNoop || uploadFile._files.length == 0
			sails.log.info "No images"
			uploadFile.upload ()->
			res.set('Content-Type', 'text/html; charset=utf-8')
			return res.badRequest('No images provided!')

		sails.log.info "Taking project ID in projectImages create ctrll"
		sails.log.info project_id
		sails.log.info req.allParams()

		res.setTimeout(0)

		if !project_id
			res.set('Content-Type', 'text/html; charset=utf-8')
			return res.badRequest('No id provided.')

		images_name = []
		files_counter = 0

		fileName = (__newFileStream, cb)->
			name = uuid.v4() + path.extname(__newFileStream.filename)
			images_name.push name
			cb null, name

		uploadLoc = "#{process.cwd()}/assets/images/Projects"

		tmpLoc = process.cwd() + '/.tmp/public/images/Projects'

		uploadFile.upload {dirname: uploadLoc, saveAs: fileName}, (err, files)->

			if err
				sails.log.error "SERVER ERROR + #{err}"
				return res.serverError(err)

			sails.log.info "Images are uplaoded ( ProjectImagesController:132)"

			for image,index in files
				nImage = {}
				nImage.image = images_name[index]
				nImage.project_id = project_id

				fs.createReadStream(uploadLoc+'/'+nImage.image).pipe(fs.createWriteStream(tmpLoc+'/'+nImage.image))

				ProjectImages.create nImage, (error, image) ->
					if error
						res.status 400
						return res.json error

					files_counter++

					if files_counter == files.length
						sails.log.info "Images are uploaded and saved ( ProjectImagesController:149)"
						res.status 200
						return res.send files

			


module.exports = ProjectImagesController

