###
SendMailController

@description :: Server-side logic for managing sendmails
@help        :: See http://links.sailsjs.org/docs/controllers
###

# nodemailer = require('nodemailer');
Mailgun = require('mailgun').Mailgun

SendMailController = 

	index: (req, res)->

		email = req.param('email')
		name = req.param('name')
		tel = req.param('tel')

		mailText = ""

		if email
			mailText = mailText + "\nВам пришел запрос на получение каталога по почте: #{email}."
		if tel && name
			mailText = mailText + "\nВам пришло уведомление о контакте с клиентом: \nИмя: #{name} \nТелефон: #{tel}"

		mg = new Mailgun('key-e95f4263230d9fba0e6e9ee51f71e2d9')

		mg.sendText('InterExpo ✔ <foo@blurdybloop.com>', ['stas@intexpo.ru'], 'Intexpo новое письмо!', mailText,
			(err)->
				if err
					console.log('Oh noes: ' + err)
					return res.serverError()
				else
					return res.ok()
		)

		


module.exports = SendMailController
