###
AdminMenuController

@description :: Server-side logic for managing adminmenus
@help        :: See http://links.sailsjs.org/docs/controllers
###

AdminMenuController = 
	index: (req, res)->
		# Admin.find (err, menu)->
			# return res.serverError(err) if err
			# res.view 'homepage', { model: menu, layout: 'layoutadmin'}
		# res.view { layout: 'layoutadmin' }
		AdminMenu.find (err, menu)->
			return res.serverError(err) if err
			res.json menu

module.exports = AdminMenuController

