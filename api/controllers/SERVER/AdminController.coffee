###
AdminController

@description :: Server-side logic for managing admins
@help        :: See http://links.sailsjs.org/docs/controllers
###

AdminController = 
	index: (req, res)->
		# Admin.find (err, menu)->
			# return res.serverError(err) if err
			# res.view 'homepage', { model: menu, layout: 'layoutadmin'}
		res.view { layout: 'layoutadmin' }

module.exports = AdminController

