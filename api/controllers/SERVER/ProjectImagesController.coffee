###
ProjectImagesController

@description :: Server-side logic for managing projectimages
@help        :: See http://links.sailsjs.org/docs/controllers
###

fs = require('fs')
path = require('path')
uuid = require('node-uuid')

ProjectImagesController =

	destroy: (req, res, next)->

		id = req.param('id')

		console.log "Destroying images"

		if !id
			res.set('Content-Type', 'text/html; charset=utf-8')
			return res.badRequest('No id provided.')

		ProjectImages.findOne(id).exec (err, result)->
			return res.serserError(err) if err

			return res.notFound() if !result

			imageName = result.image

			console.log imageName

			fs.unlinkSync("#{__dirname}/../../assets/images/Projects/"+imageName)

			ProjectImages.destroy id, (err)->

				return next(err) if err

				res.json(result)

	update: (req, res, next)->

		criteria = {}

		criteria = _.merge {}, req.params.all(), req.body

		id = req.param('id')
		mainImg = req.param 'mainImg'

		if !id
			res.set('Content-Type', 'text/html; charset=utf-8')
			return res.badRequest('No id provided.')

		setMainImg = ()->

			ProjectImages.update id, criteria, (err, result)->

				if err
					return next err

				res.status 201
				res.json result

		ProjectImages.findOne(id).populateAll().exec (err, result)->

			return res.serserError(err) if err
			return res.notFound() if !result

			project_id = result.project_id.id

			if mainImg

				ProjectImages.find {"project_id": project_id}, (err, result)->

					return next(err) if err
					return res.notFound() if result.length == 0

					# Перед тем, как назначать главное изображение, требуется поставить всем значение в false,
					# а после назначить нужное изображение
					# ! По хорошему требуется назначить false всем, КРОМЕ нужного изображения
					ProjectImages.update {"project_id": project_id}, {"mainImg": false}, (err, result)->

						console.log result
						return next(err) if err
						return res.notFound() if result.length == 0

						setMainImg()
			else
				res.status 200
				res.send 'Nothing to update'

	# 'createImages': ()
	create: (req, res, next)->

		project_id = req.param('id')

		console.log project_id
		console.log req.allParams()

		res.setTimeout(0)


		if !project_id
			res.set('Content-Type', 'text/html; charset=utf-8')
			return res.badRequest('No id provided.')

		images_name = []
		files_counter = 0

		fileName = (__newFileStream, cb)->
			name = uuid.v4() + path.extname(__newFileStream.filename)
			images_name.push name
			cb null, name

		uploadFile = req.file('uploadFile')

		console.log uploadFile
		
		console.log __dirname

		uploadLoc = "#{process.cwd()}/assets/images/Projects"

		tmpLoc = process.cwd() + '/.tmp/public/images/Projects'

		uploadFile.upload {dirname: uploadLoc, saveAs: fileName}, (err, files)->

			console.log fi = files[0].fd.substring(files[0].fd.lastIndexOf('/')+1);

			return res.serverError(err) if err

			console.log "uploaded"

			if uploadFile.isNoop
				res.set('Content-Type', 'text/html; charset=utf-8')
				return res.badRequest('No images provided!')

			console.log files

			for image,index in files
				nImage = {}
				nImage.image = images_name[index]
				nImage.project_id = project_id

				fs.createReadStream(uploadLoc+'/'+nImage.image).pipe(fs.createWriteStream(tmpLoc+'/'+nImage.image))

				ProjectImages.create nImage, (error, image) ->
					if error
						res.status 400
						return res.json error

					files_counter++

					if files_counter == files.length

						res.status 200
						return res.send files

			


module.exports = ProjectImagesController

