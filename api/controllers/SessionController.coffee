###
SessionController

@description :: Server-side logic for managing sessions
@help        :: See http://links.sailsjs.org/docs/controllers
###

bcrypt = require('bcrypt')

SessionController = 
	index: (req, res)->

		res.view { layout: 'layoutadmin' }

	'new': (req, res)->

		res.view { layout: 'layoutadmin', view: 'new' }

	'createUser': (req, res, next)->

		if (!req.param('email') || !req.param('password'))

			usernamePasswordRequireError = [{message: "Both email and password required"}]

			req.session.flash = {
				err: usernamePasswordRequireError
			}

			return res.redirect '/session/new'

		params = req.params.all()

		bcrypt.genSalt 10, (err, salt)->

			bcrypt.hash params.password, salt, (err, hash)->

				params.password = hash

				User.create params, (error, user) ->
					if error
						# serverEr = JSON.stringify error
						serverError = []
						for err of error.invalidAttributes
							obj = {}
							obj[err] = error.invalidAttributes[err][0].message
							serverError.push obj
						req.session.flash = {
							err: serverError
						}
						return res.redirect '/session/new'

					req.session.authenticated = true
					req.session.User = user

					return res.redirect '/adm'

	'login': (req, res, next)->

		if (!req.param('email') || !req.param('password'))

			usernamePasswordRequireError = [{message: "Both email and password required"}]

			req.session.flash = {
				err: usernamePasswordRequireError
			}

			return res.redirect '/session'

		User.findOneByEmail req.param('email'), (err, user)->

			return next err if err

			if (!user)

				noAccountFound = [{name: 'No account', message: "The email adress #{req.param('email')} not found"}]
				req.session.flash = {
					err: noAccountFound
				}
				return res.redirect '/session'

			bcrypt.compare req.param('password'), user.password, (err, result)->

				return next(err) if err

				if !result
					incorectPassword = [{name: 'Incorrect password', message: "You entered incorect password"}]
					req.session.flash = {
						err: incorectPassword
					}
					return res.redirect '/session'
				else
					req.session.authenticated = true
					req.session.User = user

					return res.redirect '/adm'

	'logout': (req, res, next)->

		req.session.destroy()

		return res.redirect('/session')

module.exports = SessionController

