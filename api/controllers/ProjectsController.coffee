###
ProjectsController

@description :: Server-side logic for managing projects
@help        :: See http://links.sailsjs.org/docs/controllers
###

path = require('path')
uuid = require('node-uuid')
fs = require('fs')

ProjectsController = 

	update: (req, res)->

		sails.log.info "UPDATING PROJECT INFO"

		params = {}

		params = _.merge {}, req.params.all(), req.body
		id = req.param('id')

		if !id
			res.set('Content-Type', 'text/html')
			return res.badRequest('No id provided')

		# updateProject()

		# images_name = []
		# files_counter = []

		# fileName = (__newFileStream, cb)->
		# 	name = uuid.v4() + path.extname(__newFileStream.filename)
		# 	images_name.push name
		# 	cb null, name

		# updateProject = ()->

		Projects.update(id, params).exec (err, project)->

			if err
				res.status 400
				return res.json err

			Projects.findOne(id).populateAll().exec (err, project)->

				if err
					res.status 400
					return res.json err

				sails.log.info "UPDATED INFO"

				res.status 201
				res.json project

		# createImages = (images)->

		# 	for image,index in images
		# 			image = {}
		# 			image.image = images_name[index]
		# 			image.project_id = id
		# 			# image.mainImg = true if 
		# 			ProjectImages.create image, (error, image) ->

		# 				if error
		# 					res.status 400
		# 					return res.json error

		# 				files_counter++

		# 				if files_counter == images.length

		# 					updateProject()

		# uploadFile = req.file('uploadFile')

		# if !id
		# 	res.set('Content-Type', 'text/html')
		# 	return res.badRequest('No id provided')

		# uploadFile.upload {dirname: "#{__dirname}/../../assets/images/Projects", saveAs: fileName}, (err, files) ->

		# 	if err
		# 		res.status 400
		# 		return res.json err

		# 	if files.length == 0
		# 		updateProject()
		# 	else
		# 		createImages(files)

# _____________


	# create: (req, res)->

	# 	params = {}

	# 	params = _.merge {}, req.params.all(), req.body

	# 	uploadFile = req.file('uploadFile')

	# 	images_name = []
	# 	img_counter = 0
	# 	files_counter = 0

	# 	createProject = (files)->
	# 		Projects.create params, (error, project) ->
	# 			if error
	# 				res.status 400
	# 				# console.log error
	# res.set('Content-Type', 'text/html');
	# 				return res.badRequest error
	# 				# return res.send "<textarea data-type='application/json'>#{error}</textarea>"

				# for image,index in files
				# 	image = {}
				# 	image.image = images_name[index]
				# 	image.project_id = project.id

				# 	ProjectImages.create image, (error, image) ->
				# 		if error
				# 			res.status 400
				# 			return res.json error

	# 					files_counter++

	# 					if files_counter == files.length

	# 						Projects.findOne({id: project.id}).populateAll().exec (er, project)->

	# 							if er
	# 								res.status 400
	# 								res.json er

	# 							res.status 201
	# 							res.json project


		# fileName = (__newFileStream, cb)->
		# 	name = uuid.v4() + path.extname(__newFileStream.filename)
		# 	images_name.push name
		# 	cb null, name

		# uploadFile.upload {dirname: '../../assets/images/Projects', saveAs: fileName}, (err, files)->

		# 	return res.serverError(err) if err

		# 	if uploadFile.isNoop

		# 		res.status 400
		# 		res.set('Content-Type', 'text/html');
		# 		return res.badRequest('No images provided')

		# 	# console.log files

		# 	# return res.status 400

		# 	createProject(files);

	create: (req, res, next)->

		params = {}

		params = _.merge {}, req.params.all(), req.body

		Projects.create params, (error, project) ->

			if error
				res.set('Content-Type', 'text/html');
				return res.badRequest 'Cant create project'


			Projects.findOne({id: project.id}).populateAll().exec (er, project)->

				if error
					res.set('Content-Type', 'text/html');
					return res.badRequest 'Cant find project'

				res.status 201
				res.json project

	destroy: (req, res, next)->

		project_id = req.param('id')

		if !project_id
			res.set('Content-Type', 'text/html')
			return res.badRequest('No id provided.')

		ProjectImages.find {"project_id": project_id}, (err, images)->

			return next(err) if err
			# return res.notFound() if images.length == 0

			Projects.findOne({id: project_id}).exec (err, project)->

				if err
					res.status 400
					res.json err
				if images.length != 0
					
					for image in images
						imageName = image.image
						fs.unlinkSync("#{__dirname}/../../assets/images/Projects/"+imageName)

					ProjectImages.destroy {"project_id": project_id}, (err)->

						if err
							res.status 400
							res.json err

						Projects.destroy project_id, (err)->

							if err
								res.status 400
								res.json err

							res.json(project)
				else
					Projects.destroy project_id, (err)->

							if err
								res.status 400
								res.json err

							res.json(project)

	# fileName: (__newFileStream, cb)->


	clientSide: (req, res)->

		# console.log req.session
		# console.log "_____"
		# console.log req

		Projects.find().populateAll().exec(
			(err, projects)->
				return res.serverError(err) if err
				# console.log 'Projects:'
				# console.log projects
				# model.num = Math.ceil(model.length/6)
				res.view 'homepage', { model: projects}
		)

		

module.exports = ProjectsController

